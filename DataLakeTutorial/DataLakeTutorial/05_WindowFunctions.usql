﻿@students =
    EXTRACT Student string,
            Subject string,
            Exam string,
            EventDate DateTime,
            MaxPoints int,
            PointsEarned int
    FROM "./DataLakeTutorial/students.csv"
    USING Extractors.Csv(skipFirstNRows : 1);

@students =
    SELECT Student,
           Subject,
           Exam,
           EventDate.Year AS [Year],
           ((float) PointsEarned / (float) MaxPoints) AS Ranking           
    FROM @students;

@students =
    SELECT Student,
           Subject,
           [Year],
           AVG(Ranking) OVER(PARTITION BY Student, Subject) AS AverageRankingOverall,
           AVG(Ranking) OVER(PARTITION BY Student, Subject, Year) AS AverageRankingThisYear
    FROM @students;


@students =
    SELECT DISTINCT Student,
                    Subject,
                    [Year],
                    AverageRankingOverall,
                    AverageRankingThisYear,
                    WindowFunctions.Helpers.IsBetterThanOverallAverage(AverageRankingOverall, AverageRankingThisYear) AS IsBetterThanOverall
    FROM @students;
   

OUTPUT @students
TO "./DataLakeTutorial/05_WindowFunctions_students.tsv"
ORDER BY Student, Subject, Year
USING Outputters.Tsv();
